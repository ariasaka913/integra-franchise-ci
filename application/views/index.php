<?php 
echo'';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Integra Group </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="logo3.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="html/css/app.css"/>
    <!-- end of global css -->
    <!--page level css -->
    <link href="<?php echo base_url('html/vendors/toastr/css/toastr.min.css')?>" rel="stylesheet" type="text/css"/>
    <!--weathericons-->
    <link href="<?php echo base_url('html/vendors/weathericon/css/weather-icons.min.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('html/vendors/c3/c3.min.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('html/vendors/nvd3/css/nv.d3.min.css')?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('html/css/custom.css')?>">

    <link href="<?php echo base_url('html/css/custom_css/dashboard2.css')?>" rel="stylesheet" type="text/css"/>
    <body class="skin-coreplus">
<div class="preloader">
    <div class="loader_img"><img src="<?php echo base_url('html/public/assets/img/loader.gif')?>" alt="loading..." height="64" width="64"></div>
</div>
<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-expand-md navbar-static-top">
        <a href="index " class="logo navbar-brand">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <img src="<?php echo base_url('logointegra2.png')?>" alt="logo" style="height: 50px"/>
        </a>

        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                        class="fa fa-fw fa-bars"></i>
            </a>
        </div>
    </nav>
</header>
<!-- For horizontal menu -->
<!-- horizontal menu ends -->
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="#">
                            <img src="<?php echo base_url('avatar.jpg')?>" class="rounded-circle" alt="User Image">
                        </a>
                        <div class="content-profile pl-3">
                            <h4 class="media-heading">
                              Administrator
                            </h4>
                            <ul class="icon-list list-inline">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-fw fa-user"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-fw fa-lock"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="setting" data-id="1">
                                        <i class="fa fa-fw fa-gear"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://localhost/itg/public/logout" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <i class="fa fa-fw fa-sign-out"></i>
                                    </a>
                                    <form id="logout-form" action="http://localhost/itg/public/logout" method="POST" style="display: none;">
                                        <input type="hidden" name="_token" value="sFRYVDsvoUDeusMmTGqIhk49Vqlc3zQer4gF2thR">                                    </form>                                    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                    <li >
                        <a href="<?php echo base_url('iven/index')?>">
                            <i class="menu-icon fa fa-fw fa-home"></i>
                            <span class="mm-text ">Dashboard</span>
                        </a>
                    </li>
                    
                    <li >
                        <a href="<?php echo base_url('iven/employe')?>">
                            <i class="menu-icon fa fa-fw fa-users"></i>
                            <span class="mm-text ">Employee</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo base_url('iven/customer')?>">
                            <i class="menu-icon fa fa-fw fa-users"></i>
                            <span class="mm-text ">Customer</span>
                        </a>
                    </li>
                    <li >
                        <a href="<?php echo base_url('iven/suplayer')?> ">
                            <i class="menu-icon fa fa-fw fa-users"></i>
                            <span class="mm-text ">Supplier</span>
                        </a>
                    </li>
                    <li >
                        <a href="#">
                            <i class="menu-icon fa fa-fw fa-users"></i>
                            <span class="mm-text">Users</span>
                        </a>
                        <ul class="sub-menu">
                            <li >
                                <a href="<?php echo base_url('iven/user')?>">
                                    <i class="fa fa-fw fa-user"></i> User
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/role')?>">
                                    <i class="fa fa-fw fa-fire"></i> Role
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/menu')?>">
                                    <i class="fa fa-fw fa-fire"></i> Menu
                                </a>
                            </li>
                        </ul>                        
                    </li>
                    <li >
                        <a href="#">
                            <i class="menu-icon fa fa-th"></i>
                            <span>Master</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-list-ul"></i> Term of Payment
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-list-ul"></i> Type
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-list-ul"></i> Kurs
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-list-ul"></i> Unit
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-list-ul"></i> Item
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-flag"></i> Country
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-bookmark"></i> State
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-map-marker"></i> City
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-road"></i> District
                                </a>
                            </li>
                            
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-bookmark-o"></i> Site
                                </a>
                            </li>
                            <li >
                                <a href="http://localhost/itg/public/warehouse/index">
                                    <i class="fa fa-fw fa-bookmark-o"></i> Warehouse
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-bookmark-o"></i> Location
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-th"></i> Business Unit
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-th"></i> Business Unit Branch
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-bookmark-o"></i> Division
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-bookmark-o"></i> Sub Division
                                </a>
                            </li>           
                        </ul>
                    </li>
                    <li >
                        <a href="#">
                            <i class="menu-icon fa fa-th"></i>
                            <span>Master Category</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-fire"></i> Item
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-fire"></i> Supplier
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-fire"></i> Customer
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li >
                        <a href="#">
                            <i class="menu-icon fa fa-th"></i>
                            <span>Transaction</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li >
                                <a href="<?php echo base_url('iven/')?>">
                                    <i class="fa fa-fw fa-fire"></i> Sales Order
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('iven/')?>" class="maintenance">
                                    <i class="fa fa-fw fa-fire"></i> Purchase Order
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
                <!-- / .navigation -->
            </div>
            <!-- menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <aside class="right-side">
        <!-- Content -->
        		  <!-- Content Header (Page header) -->
		  <section class="content-header">
				<h1>Customer List</h1>
				<ol class="breadcrumb">
					 <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
					 </li>
					 <li class="breadcrumb-item">
						  <a href="#">Customer</a>
					 </li>
					 <li class="breadcrumb-item active">
						Customer List
					 </li>
				</ol>
		  </section>
		  <!-- Main content -->
		  <section class="content p-l-r-15">
        <div class="row" style="display:none;">
          <div class="col-12">
            <div class="card border-primary">
              <div class="card-header text-white bg-primary">
                <h4 class="card-title d-inline">
                  <i class="fa fa-fw fa-filter"></i> Filter
                </h4>
								<span class="pull-right">
									<a href="<?php echo base_url('')?>" id="reset" class="text-white"><i class="fa fa-fw fa-refresh"></i></a>
								</span>               
              </div>
              <div class="card-body">
               

<div id="qn"></div>
<script src="<?php echo base_url('html/js/app.js')?>" type="text/javascript"></script>
<!-- end of global js -->
<!-- begining of page level js -->
<!--Sparkline Chart-->
<script src="<?php echo base_url('html/js/custom_js/sparkline/jquery.flot.spline.js')?>""></script>
<!--c3 and d3 chart-->
<script type="text/javascript" src="<?php echo base_url('html/vendors/c3/c3.min.js')?>""></script>
<script type="text/javascript" src="<?php echo base_url('html/vendors/d3/d3.min.js')?>""></script>
<!--nvd3 charts-->
<script type="text/javascript" src="<?php echo base_url('html/vendors/nvd3/js/nv.d3.min.js')?>""></script>
<!--advanced news ticker-->
<script type="text/javascript" src="<?php echo base_url('html/vendors/advanced_newsTicker/js/jquery.newsTicker.js')?>""></script>
<script src="<?php echo base_url('html/js/dashboard2.js')?>" type="text/javascript"></script>
<!-- end of page level js -->

</body>

</html>
